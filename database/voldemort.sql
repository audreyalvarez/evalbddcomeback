-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema voldemort
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema voldemort
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `voldemort` DEFAULT CHARACTER SET utf8 ;
USE `voldemort` ;

-- -----------------------------------------------------
-- Table `voldemort`.`Professeur`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `voldemort`.`Professeur` (
  `idProfesseur` INT NOT NULL,
  `Nom` VARCHAR(30) NULL,
  `Prénom` VARCHAR(30) NULL,
  `AdresseMail` VARCHAR(20) NULL,
  `Statut` ENUM('prestataire', 'salarié') NULL,
  PRIMARY KEY (`idProfesseur`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `voldemort`.`Cours`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `voldemort`.`Cours` (
  `idCours` INT GENERATED ALWAYS AS (),
  `Durée` TIME NULL,
  `cleProfesseur` VARCHAR(30) NULL,
  `IntituléDuCours` VARCHAR(45) NULL,
  PRIMARY KEY (`idCours`),
  INDEX `fk_Cours_Professeur1_idx` (`cleProfesseur` ASC),
  CONSTRAINT `fk_Cours_Professeur1`
    FOREIGN KEY (`cleProfesseur`)
    REFERENCES `voldemort`.`Professeur` (`idProfesseur`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `voldemort`.`Programme`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `voldemort`.`Programme` (
  `idProgramme` INT NOT NULL,
  `IntituléDuProgramme` VARCHAR(45) NULL,
  `cléCours` VARCHAR(45) NULL,
  PRIMARY KEY (`idProgramme`),
  INDEX `fk_Programme_Cours1_idx` (`cléCours` ASC),
  CONSTRAINT `fk_Programme_Cours1`
    FOREIGN KEY (`cléCours`)
    REFERENCES `voldemort`.`Cours` (`idCours`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `voldemort`.`eleve`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `voldemort`.`eleve` (
  `ideleve` INT NOT NULL,
  `Nom` VARCHAR(30) NULL,
  `Prénom` VARCHAR(30) NULL,
  `Adresse` VARCHAR(45) NULL,
  `Date de naissance` DATE NULL,
  `Date d'inscription` DATE NULL,
  `cléProgramme` VARCHAR(45) NULL,
  PRIMARY KEY (`ideleve`),
  INDEX `fk_eleve_Programme1_idx` (`cléProgramme` ASC),
  CONSTRAINT `fk_eleve_Programme1`
    FOREIGN KEY (`cléProgramme`)
    REFERENCES `voldemort`.`Programme` (`idProgramme`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `voldemort`.`Professeur`
-- -----------------------------------------------------
START TRANSACTION;
USE `voldemort`;
INSERT INTO `voldemort`.`Professeur` (`idProfesseur`, `Nom`, `Prénom`, `AdresseMail`, `Statut`) VALUES (DEFAULT, 'Garcia', 'Jose', 'jose.g@gmail.com', 'salarié');

COMMIT;


-- -----------------------------------------------------
-- Data for table `voldemort`.`Cours`
-- -----------------------------------------------------
START TRANSACTION;
USE `voldemort`;
INSERT INTO `voldemort`.`Cours` (`idCours`, `Durée`, `cleProfesseur`, `IntituléDuCours`) VALUES (DEFAULT, '01:00:00', NULL, NULL);
INSERT INTO `voldemort`.`Cours` (`idCours`, `Durée`, `cleProfesseur`, `IntituléDuCours`) VALUES (DEFAULT, '01:00:00', NULL, NULL);
INSERT INTO `voldemort`.`Cours` (`idCours`, `Durée`, `cleProfesseur`, `IntituléDuCours`) VALUES (DEFAULT, '01:00:00', NULL, NULL);
INSERT INTO `voldemort`.`Cours` (`idCours`, `Durée`, `cleProfesseur`, `IntituléDuCours`) VALUES (DEFAULT, '01:00:00', NULL, NULL);
INSERT INTO `voldemort`.`Cours` (`idCours`, `Durée`, `cleProfesseur`, `IntituléDuCours`) VALUES (DEFAULT, '01:00:00', NULL, NULL);
INSERT INTO `voldemort`.`Cours` (`idCours`, `Durée`, `cleProfesseur`, `IntituléDuCours`) VALUES (DEFAULT, '01:00:00', NULL, NULL);
INSERT INTO `voldemort`.`Cours` (`idCours`, `Durée`, `cleProfesseur`, `IntituléDuCours`) VALUES (DEFAULT, '01:00:00', NULL, NULL);
INSERT INTO `voldemort`.`Cours` (`idCours`, `Durée`, `cleProfesseur`, `IntituléDuCours`) VALUES (DEFAULT, '01:00:00', NULL, NULL);
INSERT INTO `voldemort`.`Cours` (`idCours`, `Durée`, `cleProfesseur`, `IntituléDuCours`) VALUES (DEFAULT, '01:00:00', NULL, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `voldemort`.`Programme`
-- -----------------------------------------------------
START TRANSACTION;
USE `voldemort`;
INSERT INTO `voldemort`.`Programme` (`idProgramme`, `IntituléDuProgramme`, `cléCours`) VALUES (, '', NULL);
INSERT INTO `voldemort`.`Programme` (`idProgramme`, `IntituléDuProgramme`, `cléCours`) VALUES (, '', NULL);
INSERT INTO `voldemort`.`Programme` (`idProgramme`, `IntituléDuProgramme`, `cléCours`) VALUES (, '', NULL);
INSERT INTO `voldemort`.`Programme` (`idProgramme`, `IntituléDuProgramme`, `cléCours`) VALUES (, '', NULL);
INSERT INTO `voldemort`.`Programme` (`idProgramme`, `IntituléDuProgramme`, `cléCours`) VALUES (, '', NULL);
INSERT INTO `voldemort`.`Programme` (`idProgramme`, `IntituléDuProgramme`, `cléCours`) VALUES (, '', NULL);
INSERT INTO `voldemort`.`Programme` (`idProgramme`, `IntituléDuProgramme`, `cléCours`) VALUES (, '', NULL);
INSERT INTO `voldemort`.`Programme` (`idProgramme`, `IntituléDuProgramme`, `cléCours`) VALUES (, '', NULL);
INSERT INTO `voldemort`.`Programme` (`idProgramme`, `IntituléDuProgramme`, `cléCours`) VALUES (, '', NULL);
INSERT INTO `voldemort`.`Programme` (`idProgramme`, `IntituléDuProgramme`, `cléCours`) VALUES (, '', NULL);
INSERT INTO `voldemort`.`Programme` (`idProgramme`, `IntituléDuProgramme`, `cléCours`) VALUES (, '', NULL);

COMMIT;

